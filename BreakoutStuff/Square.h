//
//  Square.h
//  BreakoutStuff
//
//  Created by James Cash on 10-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Rectangle.h"

@interface Square : Rectangle

- (instancetype)initWithWidth:(CGFloat)width;

@end
