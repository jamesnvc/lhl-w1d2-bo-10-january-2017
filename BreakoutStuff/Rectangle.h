//
//  Rectangle.h
//  BreakoutStuff
//
//  Created by James Cash on 10-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Rectangle : NSObject
//{
//    CGFloat _width;
//}

@property (nonatomic,assign) CGFloat width;
//- (CGFloat)width;
//- (void)setWidth:(CGFloat)newWidth;
@property (nonatomic,assign) CGFloat height;
@property (nonatomic,strong) NSString *colour;

- (instancetype)initWithWidth:(CGFloat)width height:(CGFloat)height colour:(NSString*)colour;
- (CGFloat)area;

@end
