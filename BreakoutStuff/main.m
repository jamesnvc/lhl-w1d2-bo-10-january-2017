//
//  main.m
//  BreakoutStuff
//
//  Created by James Cash on 10-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Rectangle.h"
#import "Square.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {

        Rectangle *rect1 = [[Rectangle alloc] initWithWidth:100
                                                     height:50
                                                     colour:@"Red"];

        NSLog(@"Rectangle 1's area is %g", rect1.area);

        Square *square1 = [[Square alloc] initWithWidth:50];
        NSLog(@"Square 1's area is %g", square1.area);

        square1.height = 100; // [square1 setHeight:100]
        NSLog(@"Square 1's area is %g", square1.area);

        // NSMutableArray
        NSMutableArray *rectangles = [[NSMutableArray alloc] init];
        NSLog(@"Array starts with %ld items", [rectangles count]);
        [rectangles addObject:rect1];
        [rectangles addObject:square1];
        NSLog(@"Array now has %ld items", [rectangles count]);

        for (Rectangle* r in rectangles) {
            NSLog(@"This rectangle has area %g", [r area]);
        }

        [rectangles removeObjectAtIndex:0];
        for (Rectangle* r in rectangles) {
            NSLog(@"This rectangle has area %g", [r area]);
        }
    }
    return 0;
}
