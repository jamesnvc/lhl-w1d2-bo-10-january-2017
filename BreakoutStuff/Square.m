//
//  Square.m
//  BreakoutStuff
//
//  Created by James Cash on 10-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Square.h"

@implementation Square

- (instancetype)initWithWidth:(CGFloat)width
{
    NSLog(@"Initing a square");
    self = [super initWithWidth:width height:width colour:@"Aquamarine"];
    if (self) {

    }
    return self;
}

- (void)setWidth:(CGFloat)newWidth
{
    [super setWidth:newWidth];
    [super setHeight:newWidth];

}

- (void)setHeight:(CGFloat)newHeight
{
    [super setHeight:newHeight];
    [super setWidth:newHeight];
}

@end
