
//
//  Rectangle.m
//  BreakoutStuff
//
//  Created by James Cash on 10-01-17.
//  Copyright © 2017 Occasionally Cogent. All rights reserved.
//

#import "Rectangle.h"

@implementation Rectangle

- (instancetype)initWithWidth:(CGFloat)width height:(CGFloat)height colour:(NSString*)colour
{
    NSLog(@"Initing a rectangle");
    self = [super init];
    if (self) {
        _width = width;
        _height = height;
        _colour = colour;
    }
    return self;
}

- (CGFloat)area
{
//    return [self width] * [self height];
    return self.width * self.height;
}

@end
